from gpiozero import LED
from http.server import BaseHTTPRequestHandler, HTTPServer
import threading

class RequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/led/on':
            led.on()
            self.send_response(200)
            self.end_headers()
            self.wfile.write(b"LED ON")
        elif self.path == '/led/off':
            led.off()
            self.send_response(200)
            self.end_headers()
            self.wfile.write(b"LED OFF")
        else:
            self.send_response(404)
            self.end_headers()
            self.wfile.write(b"Not Found")

def run_server():
    server_address = ('', 8000)
    httpd = HTTPServer(server_address, RequestHandler)
    httpd.serve_forever()

led = LED(17)

server_thread = threading.Thread(target=run_server)
server_thread.daemon = True
server_thread.start()

print("Server started at http://localhost:8000")
print("Send GET request to http://localhost:8000/led/on to turn LED on")
print("Send GET request to http://localhost:8000/led/off to turn LED off")

server_thread.join()